package com.example.coursework2app;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {

    private EditText stock;
    private Spinner beginDay;
    private Spinner beginMonth;
    private Spinner beginYear;
    private Spinner endDay;
    private Spinner endMonth;
    private Spinner endYear;
    private Spinner intervalBox;
    private String[] days = new String[31];
    private String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Dec"};
    private String[] years = new String[44];
    private String[] interval = {"Monthly", "Daily", "Weekly"};
    private String[] colNames = {"Date", "Open", "High", "Low", "Close", "Volume", "Adj Close"};
    private CSVReader csv;
    List<String[]> stockData;
    protected static String fullTable;
    protected static String windowTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        stock = (EditText) findViewById(R.id.editText);
        beginDay = (Spinner) findViewById(R.id.spinner);
        beginMonth = (Spinner) findViewById(R.id.spinner2);
        beginYear = (Spinner) findViewById(R.id.spinner3);
        endDay = (Spinner) findViewById(R.id.spinner4);
        endMonth = (Spinner) findViewById(R.id.spinner5);
        endYear = (Spinner) findViewById(R.id.spinner6);
        intervalBox = (Spinner) findViewById(R.id.spinner7);
        boxValues();

        ArrayAdapter<String> dayArrayAdapter = new ArrayAdapter<String>(this,   android.R.layout.simple_spinner_dropdown_item, days);
        dayArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ArrayAdapter<String> monthArrayAdapter = new ArrayAdapter<String>(this,   android.R.layout.simple_spinner_dropdown_item, months);
        monthArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);// The drop down view

        ArrayAdapter<String> yearArrayAdapter = new ArrayAdapter<String>(this,   android.R.layout.simple_spinner_dropdown_item, years);
        yearArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ArrayAdapter<String> intervalArrayAdapter = new ArrayAdapter<String>(this,   android.R.layout.simple_spinner_dropdown_item, interval);
        intervalArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        beginDay.setAdapter(dayArrayAdapter);
        endDay.setAdapter(dayArrayAdapter);
        beginMonth.setAdapter(monthArrayAdapter);
        endMonth.setAdapter(monthArrayAdapter);
        beginYear.setAdapter(yearArrayAdapter);
        endYear.setAdapter(yearArrayAdapter);
        intervalBox.setAdapter(intervalArrayAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private void boxValues(){
        int a = 1;
        for(int i = 0; i < 31; i++){
            days[i] = a + "";
            a++;
        }

        int z = 1970;
        int x = 0;

        while(z <= 2013 && x <= 43){
            years[x] = z + "";
            x++; z++;
        }
    }

    public void lookUp(View view) throws InterruptedException, IOException {

        String x = stock.getText().toString();
        String x1 = (String) beginYear.getSelectedItem();
        String x2 = (String) endYear.getSelectedItem();
        getData("http://ichart.yahoo.com/table.csv?s=" + x + "&a=" + beginMonth.getSelectedItemPosition() + "&b=" + (beginDay.getSelectedItemPosition()+1)
                + "&c=" + x1 + "&d=" + endMonth.getSelectedItemPosition() + "&e=" + (endDay.getSelectedItemPosition()+1) + "&f=" + x2 + "&g=" + getInterval(intervalBox));

        windowTitle = x + ": " + x1 + "/" + (String)beginMonth.getSelectedItem() + "/" + (beginDay.getSelectedItemPosition()+1) + " to "
                + x2 + "/" + (String)endMonth.getSelectedItem() + "/" + (endDay.getSelectedItemPosition()+1) + " (" + (String)intervalBox.getSelectedItem() + ")";

        /*All loops in reverse order (iterating from high to low) to ensure columns are in correct order (Date first etc)
        For some strange reason, html is reading backwards (in reverse)*/
        
        String topRow = "";
        String row = "";

        for(int i = colNames.length-1; i >=0 ; i--){
            topRow =  "<td>" + colNames[i] + "</td>" + topRow;
        }

        topRow = "<tr>" + topRow + "</tr>";
        ArrayList<String> listOfRows = new ArrayList<String>();
        Double[] adjClose = new Double[stockData.size()-1];
        Double[] copy = new Double[adjClose.length];

        for(int i = 1; i < stockData.size(); i++)
        {
            String[] rowData = stockData.get(i);
            adjClose[i-1] = Double.parseDouble(rowData[6]);
            copy[i-1] = Double.parseDouble(rowData[6]);
        }


        for(int i = adjClose.length-1; i > 0; i--)
        {
            if(adjClose[i] > adjClose[i-1]){
                copy[i-1] = 0.0; //red
            }
            else if(adjClose[i] < adjClose[i-1]){
                copy[i-1] = 1.0; //green
            }
        }

        for(int i = stockData.size()-1; i >= 1 ; i--){
            String[] currentRow = stockData.get(i);
            row = "<tr>" + row;

            for(int j = currentRow.length-1; j >= 0 ; j--){

                if(j == 6){
                    if(copy[i-1] == 0.0){
                        row = "<td style=\"color: #ff0000;\">" + currentRow[j] + "</td>" + row;
                    }
                    else if(copy[i-1] == 1.0){
                        row = "<td style=\"color: #00ff00;\">" + currentRow[j] + "</td>" + row;
                    }
                    else{
                        row = "<td>" + currentRow[j] + "</td>" + row;
                    }
                }
                else{
                    row = "<td>" + currentRow[j] + "</td>" + row;
                }
            }
             listOfRows.add(row + "</tr>");
             row = "";
        }

        for(int i = listOfRows.size()-1; i >= 0 ; i--){
            row = listOfRows.get(i) + row;
        }

        fullTable = "<html><table border=1 bordercolor=white style=\"color: #fff;\">" + topRow + row + "</table></html>";
        Intent i=new Intent(MainActivity.this,ResultActivity.class);
        startActivity(i);

    }

    public void getData(final String url) throws InterruptedException{ //updates the csv reader with url data

        Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {
                try{

                    HttpClient httpclient = new DefaultHttpClient();
                    HttpGet request = new HttpGet(url);
                    // Execute HTTP GET Request
                    HttpResponse response = httpclient.execute(request);
                    BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                    csv = new CSVReader(br);
                    stockData = csv.readAll();

                } catch (ClientProtocolException e) {
                    // TODO Auto-generated catch block
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                }
            }

        });

        thread.start();
        thread.join();
    }

    private String getInterval(Spinner s){

        String intervalString;

        if(s.getSelectedItemPosition() == 0){
            intervalString = "m";
        }
        else if(s.getSelectedItemPosition() == 1){
            intervalString = "d";
        }
        else{
            intervalString = "w";
        }
        return intervalString;
    }

}
