package com.example.coursework2app;

import android.graphics.Color;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.webkit.WebView;


public class ResultActivity extends Activity {

    private WebView table;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        table = (WebView) findViewById(R.id.webView);
        table.setBackgroundColor(Color.TRANSPARENT);
        table.loadData(MainActivity.fullTable, "text/html", "UTF-8");
        setTitle(MainActivity.windowTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.result, menu);
        return true;
    }
    
}
